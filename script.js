let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu","Charizard", "Squirtle","Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		console.log("Pikachu! I choose you")
	}
}
console.log(trainer);

console.log("Result of dot notation: ");
console.log(trainer.name);

console.log("Result of square bracket notation:")
console.log(trainer.pokemon);

console.log("Result of talk method");
trainer.talk();

// Number 2 Pokemon Constructor

function pokemon(name, level, health, attack){
	this.name = name;
	this.level = level;
	this.health = level * 3;
	this.attack = level * 1.5;

	this.tackle = function(targetPokemon){
		console.log(this.name + " tackled" + targetPokemon );
	}
	this.faint = function(){
		console.log(this.name + " has fainted!");
	}
}

let pokemon1 = new pokemon("Pikachu" , 12);
console.log(pokemon1);

let pokemon2  = new pokemon("Geodude", 8);
console.log(pokemon2);

let pokemon3 = new pokemon("Meowtwo", 100);
console.log(pokemon3);


// Number 3

console.log(pokemon2.name + " tackled " + pokemon1.name + ". ");
console.log(pokemon3.name + " tackled " + pokemon2.name + ". ");
console.log(pokemon3.name + "fainted");
